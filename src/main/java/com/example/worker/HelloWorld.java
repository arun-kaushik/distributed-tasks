package com.example.worker;

import com.example.app.App;
import com.example.app.Slave;
import com.example.app.Job;
import com.example.app.Task;
import com.example.app.constants.Statuses;
import org.json.JSONObject;

import java.io.*;
import java.util.HashMap;
import java.util.logging.Level;

public class HelloWorld implements WorkerBehavior {
    private JSONObject params;
    private final String resultDir = "/Users/arun.kaushik/Documents/distributed-tasks/jobs/";
    private final String resultFileName = "result.txt";

    public HelloWorld(JSONObject params) {
        this.params = params;
    }
    public HelloWorld(){
        this.params = null;
    }

    @Override
    public void splitJob(Job job) throws Exception {
        int helloCount = 100;
        while(helloCount-- > 0){
            HashMap<String, String > details = new HashMap<String, String>();
            details.put("greetings", "HelloWorld");
            job.addTask(new Task(details));
        }

    }

    @Override
    public void reduceResults(Job job) throws Exception {
        job.updateStatus(Statuses.REDUCING_RESULTS);
        String finalResult = "";
        try {
            for(Task task : job.getTasks()){
                finalResult = finalResult + task.getResult() + "\n";
            }

            String directoryName = resultDir.concat(job.getUuid());

            File directory = new File(directoryName);
            if (! directory.exists()){
                directory.mkdirs();
            }

            File file = new File(directoryName + "/" + resultFileName);
            try{
                FileWriter fw = new FileWriter(file.getAbsoluteFile());
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write(finalResult);
                bw.close();
            }
            catch (IOException e){
                App.logger.log(Level.WARNING, "Failed to write the results of job: " + job.getUuid() + " to: " + file.getAbsoluteFile() );
                App.logger.log(Level.WARNING, e.getStackTrace().toString());
            }

        } catch (Exception e){
            App.logger.log(Level.WARNING, "Failed to reduce the results for job: " + job.getUuid());
            App.logger.log(Level.WARNING, e.getStackTrace().toString());
        }
    }

    @Override
    public String perform(String taskDetails) {
        JSONObject taskObj = new JSONObject(taskDetails);
        String greetings = taskObj.get("greetings").toString();
        return greetings + " from master at: " + System.nanoTime();
    }
}