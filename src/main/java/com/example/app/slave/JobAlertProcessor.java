package com.example.app.slave;

import com.example.app.App;
import com.example.app.master.Registrar;
import com.example.app.network.SocketConnection;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;

class JobAlertProcessor implements Runnable {
    private SocketConnection jobAlertConnection;

    public JobAlertProcessor(Socket socket) {
        try {
            this.jobAlertConnection = new SocketConnection(socket);
        } catch (Exception e){
            App.logger.log(Level.SEVERE, "Failed to open input/output buffers for socket: " + socket);
            App.logger.log(Level.SEVERE, e.getStackTrace().toString());
            try { socket.close(); } catch (IOException ex) {}
        }
    }

    @Override
    public void run() {
        try {
            while(!Thread.currentThread().isInterrupted()){
                String jobId = jobAlertConnection.readMessage();
                App.logger.log(Level.INFO, "Job alert for jobId: " + jobId);
                jobAlertConnection.writeMessage("OK");
                jobAlertConnection.flush();
                Thread registrarThread = new Thread(new TaskExecutor(jobId));
                registrarThread.setName("Job: " + jobId);
                registrarThread.start();
            }
        } catch (Exception e) {
            App.logger.log(Level.SEVERE, "Error while processing job alert from the socket: " + jobAlertConnection.getSocket());
            App.logger.log(Level.SEVERE, "Error:" + e.getStackTrace());
        } finally {
            try { jobAlertConnection.close(); } catch (IOException e) {}
        }
    }
}