package com.example.app.master;

import com.example.app.App;

import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TaskRequestReceiver implements Runnable{

    public TaskRequestReceiver() {
    }
    @Override
    public void run() {
        try {
            ServerSocket listener = new ServerSocket(App.MASTER_REQUEST_RECEIVER_PORT);
            ExecutorService registrarPool = Executors.newCachedThreadPool();
            while (true) {
                registrarPool.execute(new TaskBroker(listener.accept()));
            }
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
