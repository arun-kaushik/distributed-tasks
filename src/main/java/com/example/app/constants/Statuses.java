package com.example.app.constants;

public enum Statuses {
    RECEIVED, REJECTED, BROAD_CASTED, IN_PROGRESS, TASKS_CONVERGED, REDUCING_RESULTS, COMPLETED, FAILED
}
