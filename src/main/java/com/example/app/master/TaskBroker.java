package com.example.app.master;

import com.example.app.App;
import com.example.app.Job;
import com.example.app.Slave;
import com.example.app.Task;
import com.example.app.constants.Statuses;
import com.example.app.network.SocketConnection;
import org.json.JSONObject;

import javax.swing.plaf.TableHeaderUI;
import java.io.*;
import java.net.Socket;
import java.util.HashMap;
import java.util.Scanner;
import java.util.logging.Level;

public class TaskBroker implements Runnable {
    private SocketConnection taskDelegationConnection;

    public TaskBroker(Socket socket) {
        try {
             this.taskDelegationConnection = new SocketConnection(socket);
        } catch (Exception e){
            App.logger.log(Level.SEVERE, "Failed to open input/output buffers for socket: " + socket);
            App.logger.log(Level.SEVERE, e.getStackTrace().toString());
            try { socket.close(); } catch (IOException ex) {}
        }
    }

    @Override
    public void run() {
        try {
            while(!Thread.currentThread().isInterrupted()){
                String message = taskDelegationConnection.readMessage();
                HashMap<String, String> requestDetails = getRequestInfo(message);
                App.logger.log(Level.INFO, "Task request is received from slave: " + requestDetails.get("slaveId") +
                        " for job: " + requestDetails.get("jobId"));
                if (!App.jobList.containsKey(requestDetails.get("jobId"))){
                    App.logger.log(Level.INFO, "No job exists with id: " + requestDetails.get("jobId"));
                } else if(!App.slaveList.containsKey(requestDetails.get("slaveId"))){
                    App.logger.log(Level.INFO, "No such slave is registered: " + requestDetails.get("slaveId"));
                } else {
                    Job job = App.jobList.get(requestDetails.get("jobId"));
                    Slave slave = App.slaveList.get(requestDetails.get("slaveId"));
                    if(job.getStatus() == Statuses.COMPLETED || job.getStatus() == Statuses.FAILED){
                        App.logger.log(Level.INFO, "Closing the thread since job status changed to: " + job.getStatus().toString());
                        handleJobDelegation(job, slave);
                        Thread.sleep(2000);
                        taskDelegationConnection.close();
                        Thread.currentThread().interrupt();
                        return;
                    }
                    handleJobDelegation(job, slave);
                }
            }
        } catch (Exception e) {
            System.out.println("Error:" + e.getMessage());
//            e.printStackTrace();
        } finally {
            try { taskDelegationConnection.close(); } catch (IOException e) {}
        }
    }

    private void handleJobDelegation(Job job, Slave slave){
        HashMap<String, String> taskDetails = new HashMap<>();
        taskDetails.put("jobStatus", job.getStatus().toString());
        Task task = null;

        if(job.getStatus() == Statuses.BROAD_CASTED || job.getStatus() == Statuses.IN_PROGRESS ||
                job.getStatus() == Statuses.RECEIVED){
            task = job.assignTaskToSlave(slave);
            String taskJson = task == null ? "-" : task.toJson();
            taskDetails.put("task", taskJson);
        }
        JSONObject messageJson = new JSONObject(taskDetails);

        try{
            taskDelegationConnection.writeMessage(messageJson.toString());
            taskDelegationConnection.flush();
            String response = taskDelegationConnection.readMessage();
            App.logger.log(Level.INFO, "RESPONSE: " + response);
            if("OK".equals(response)){
                App.logger.log(Level.INFO, "Slave: " + slave.getUuid() + "accepted task for job: " + job.getUuid());
                if(job.getStatus() == Statuses.BROAD_CASTED){
                    job.updateStatus(Statuses.IN_PROGRESS);
                }
            } else {
                App.logger.log(Level.WARNING, "Slave: " + slave.getUuid() + "rejected task: " + task.getUuid() + " for job: " + job.getUuid());
                if(task != null){
                    task.revokeFromSlave(slave);
                }
            }
        } catch (IOException e){
            App.logger.log(Level.WARNING, "Failed to delegate a task from job: " + job.getUuid() + "Slave: " + slave.getUuid());
            if(task != null){
                task.revokeFromSlave(slave);
            }
        }
    }

    private HashMap<String, String > getRequestInfo(String message){
        HashMap<String, String> result = new HashMap<>();
        JSONObject obj = new JSONObject(message);
        result.put("slaveId", obj.getString("slaveId"));
        result.put("jobId", obj.getString("jobId"));
        return result;
    }
}