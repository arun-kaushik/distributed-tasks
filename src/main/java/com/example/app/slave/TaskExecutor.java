package com.example.app.slave;

import com.example.app.App;
import com.example.app.constants.Statuses;
import com.example.app.network.SocketConnection;
import com.example.worker.Capitalizer;
import com.example.worker.HelloWorld;
import com.example.worker.JenkinsScriptExecutor;
import com.example.worker.WorkerBehavior;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;

class TaskExecutor implements Runnable {
    private final String jobId;
    private SocketConnection taskFetchConnection;
    private SocketConnection resultSendConnection;

    public TaskExecutor(String jobId) {
        this.jobId = jobId;
        try {
            taskFetchConnection = new SocketConnection(App.masterIp, App.MASTER_REQUEST_RECEIVER_PORT);
            resultSendConnection = new SocketConnection(App.masterIp, App.MASTER_RESULT_RECEIVER_PORT);
        } catch (IOException e){
            App.logger.log(Level.SEVERE, Thread.currentThread().getName() + ": Connection to master: " + App.masterIp + " failed");
            App.logger.log(Level.SEVERE, e.getCause().toString());
        }
    }

    @Override
    public void run() {
        String msg;
        try {
            while(!Thread.currentThread().isInterrupted() && taskFetchConnection.isConnected()){
                App.logger.log(Level.INFO, "Requesting task for job: " + jobId);
                taskFetchConnection.writeMessage(getTaskRequestJson());
                taskFetchConnection.flush();
                msg = taskFetchConnection.readMessage();
                taskFetchConnection.writeMessage("OK");
                taskFetchConnection.flush();
                JSONObject taskObj = new JSONObject(msg);
                String status = taskObj.get("jobStatus").toString();
                if (status.equals(Statuses.COMPLETED.toString()) || status.equals(Statuses.FAILED.toString())){
                    App.logger.log(Level.INFO, "Job with jobId: " + jobId + " has no more tasks. Closing the Thread: " +
                                                      Thread.currentThread().getName());
                    closeConnections();
                    Thread.currentThread().interrupt();
                    return;
                } else {
                    String result = performTask(taskObj);
                    resultSendConnection.writeMessage(result);
                    resultSendConnection.flush();
                    String response = resultSendConnection.readMessage();

                    if("OK".equals(response)){
                        App.logger.log(Level.INFO, "Successfully reported task result to master.");
                    } else {
                        App.logger.log(Level.INFO, "Failed to report task result to master.");
                    }
                }
            }
        } catch (Exception e) {
            App.logger.log(Level.SEVERE, "Error while processing tasks for job: " + jobId);
            App.logger.log(Level.SEVERE, "Error:" + e.getStackTrace());
        } finally {
            closeConnections();
        }
    }

    private String getTaskRequestJson(){
        HashMap<String, String> request = new HashMap<>();
        request.put("slaveId", App.slaveId);
        request.put("jobId", jobId);
        JSONObject requestjson = new JSONObject(request);
        return requestjson.toString();
    }

    private String performTask(JSONObject taskObj){
        JSONObject taskJson = new JSONObject(taskObj.get("task").toString());
        String workerName = taskJson.get("workerName").toString();
        String taskDetails = taskJson.get("taskDetails").toString();
        HashMap<String, String> taskResult = new HashMap<String, String>();
        taskResult.put("slaveId", App.slaveId);
        taskResult.put("jobId", jobId);
        taskResult.put("taskId", taskJson.get("uuid").toString());
        try {
            WorkerBehavior workerBehavior = getWorkerBehaviorForTask(workerName);
            String result = workerBehavior.perform(taskDetails);
            taskResult.put("result", result);
            taskResult.put("status", "success");
        } catch (Exception e){
            taskResult.put("result", e.getMessage());
            taskResult.put("status", "failure");
        }
        JSONObject json = new JSONObject(taskResult);
        return json.toString();
    }

    private WorkerBehavior getWorkerBehaviorForTask(String workerName) throws Exception{
        workerName = workerName.toUpperCase();
        switch (workerName){
            case "HELLOWORLD":
                return new HelloWorld();
            case "CAPITALIZER":
                return new Capitalizer();
            case "JENKINSSCRIPTEXECUTOR":
                return new JenkinsScriptExecutor();
            default:
                throw new Exception("illegal worker");
        }
    }

    private void closeConnections(){
        try{
            taskFetchConnection.close();
            resultSendConnection.close();
        }  catch (IOException e) {}
    }
}