package com.example.worker;

import com.example.app.App;
import com.example.app.Slave;
import com.example.app.Job;
import com.example.app.Task;
import com.example.app.constants.Statuses;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.HashMap;
import java.util.logging.Level;

public class JenkinsScriptExecutor implements WorkerBehavior {
    private JSONObject params;
    private final String resultDir = "/Users/arun.kaushik/Documents/distributed-tasks/jobs/";

    public JenkinsScriptExecutor(JSONObject params) {
        this.params = params;
    }

    public JenkinsScriptExecutor() {
        this.params = null;
    }

    @Override
    public void splitJob(Job job) throws Exception {
        JSONObject params = job.getParams();
        String scriptFilePath = params.get("scriptFilePath").toString();
        String jenkinsScript = new String(Files.readAllBytes(Paths.get(scriptFilePath)), StandardCharsets.UTF_8);

        String jenkinsInstancesFilePath = params.get("instancesFilePath").toString();
        File file = new File(jenkinsInstancesFilePath);

        String gitToken = params.get("gitToken").toString();

        try(BufferedReader br = new BufferedReader(new FileReader(file))) {
            for(String line; (line = br.readLine()) != null; ) {
                String instance = line.trim();
                if(instance == null || instance.isEmpty())
                    continue;
                HashMap<String, String > details = new HashMap<String, String>();
                details.put("script", jenkinsScript );
                details.put("instanceUrl", line.trim());
                details.put("gitToken", gitToken);
                job.addTask(new Task(details));
            }
        }
    }

    @Override
    public void reduceResults(Job job) throws Exception {
        job.updateStatus(Statuses.REDUCING_RESULTS);
        try {
            for(String taskId : job.getTaskOrder()){
                Task task = job.getTaskwithId(taskId);
                String instanceUrl = task.getDetail("instanceUrl");
                String result = task.getResult();
                String directoryName = resultDir.concat(job.getUuid());

                File directory = new File(directoryName);
                if (! directory.exists()){
                    directory.mkdirs();
                }

                File file = new File(directoryName + "/" + instanceUrl + ".txt");
                try{
                    FileWriter fw = new FileWriter(file.getAbsoluteFile());
                    BufferedWriter bw = new BufferedWriter(fw);
                    bw.write(result);
                    bw.close();
                }
                catch (IOException e){
                    App.logger.log(Level.WARNING, "Failed to write the results of job: " + job.getUuid() + " to: " + file.getAbsoluteFile() );
                    App.logger.log(Level.WARNING, e.getStackTrace().toString());
                }
            }

        } catch (Exception e){
            App.logger.log(Level.WARNING, "Failed to reduce the results for job: " + job.getUuid());
            App.logger.log(Level.WARNING, e.getStackTrace().toString());
        }
    }

    @Override
    public String perform(String taskDetails) throws Exception{
        JSONObject taskObj = new JSONObject(taskDetails);
        String script = taskObj.get("script").toString();
        String instanceUrl = taskObj.get("instanceUrl").toString();
        String gitToken = taskObj.get("gitToken").toString();

        String crumb = getJenkinsCrumb(instanceUrl, gitToken);
        return executeScript(script, instanceUrl, gitToken, crumb);
    }

    private String getJenkinsCrumb(String instanceUrl, String gitToken) throws Exception{
        String url = "https://" + instanceUrl + "/crumbIssuer/api/json/";
        String response = doGet(url, gitToken);
        JSONObject crumbObj = new JSONObject(response);
        return crumbObj.get("crumb").toString();
    }

    private String executeScript(String script, String instanceUrl, String gitToken, String crumb) throws IOException{
        String url = "https://" + instanceUrl + "/scriptText/";
        return sendPOST(url, script, gitToken, crumb);
    }

    private String doGet(String url, String gitToken) throws IOException {
        URL obj = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();
        connection.setRequestMethod("GET");
        String userCredentials = "X:" + gitToken;
        String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userCredentials.getBytes()));

        connection.setRequestProperty ("Authorization", basicAuth);
        int responseCode = connection.getResponseCode();
        App.logger.log(Level.INFO, "Jenkins crumb request returned http code: " + responseCode);

        if (responseCode == HttpURLConnection.HTTP_OK) {
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            return response.toString();
        } else {
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            App.logger.log(Level.INFO, "RESPONSE: "  + response.toString());
            throw new IOException("Failed to get Jenkins crumb: " + responseCode);
        }

    }

    private String sendPOST(String url, String script,String gitToken, String crumb ) throws IOException {
        URL obj = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) obj.openConnection();
        connection.setRequestMethod("POST");
        String userCredentials = "X:" + gitToken;
        String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userCredentials.getBytes()));

        connection.setRequestProperty ("Authorization", basicAuth);

        connection.setDoOutput(true);
        connection.setRequestProperty ("Jenkins-Crumb", crumb);
        OutputStream os = connection.getOutputStream();
        os.write(("script=" + script).getBytes());
        os.flush();
        os.close();

        int responseCode = connection.getResponseCode();
        App.logger.log(Level.INFO, "Jenkins script execution request returned http code: " + responseCode);

        if (responseCode == HttpURLConnection.HTTP_OK) {
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            return response.toString();
        } else {
            throw new IOException("Failed to execute Jenkins script: " + responseCode);
        }
    }
}