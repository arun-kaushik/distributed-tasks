package com.example.app.master;

import com.example.app.App;

import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class JobReceiver implements Runnable{

    public JobReceiver() {
    }
    @Override
    public void run() {
        try {
            ServerSocket listener = new ServerSocket(App.MASTER_JOBS_RECEIVER_PORT);
            ExecutorService registrarPool = Executors.newFixedThreadPool(5);
            while (true) {
                registrarPool.execute(new JobBroker(listener.accept()));
            }
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
