/*
This class is responsible to receive the job from the JobBroker, find the appropriate strategy and broadcasting it to
the slaves. It creates an Job object and saves it reference to reconcile the results once slaves converge with results.
* */
package com.example.app;

import com.example.app.constants.Statuses;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.logging.Level;

public class Marshal {
    JSONObject params;
    String errorMessage;

    public Marshal(String args){
        this.errorMessage = null;
        try{
            this.params = new JSONObject(args);
        } catch (Exception e){
            this.errorMessage = e.getMessage();
        }
    }

    public String execute(){
        if(errorMessage != null){
            return errorResponse();
        }

        if("NEWJOB".equals( params.get("cmd").toString().toUpperCase() )){
            Job job = new Job(params);
            return handleJobExecutions(job);
        } else {
            Command cmd = new Command(params);
            return handleCommandExecutions(cmd);
        }
    }

    private String errorResponse(){
        HashMap<String, String> result = new HashMap<>();
        result.put("status", "failure");
        result.put("errorMeassage", errorMessage);
        return new JSONObject(result).toString();
    }

    public String handleJobExecutions(Job job){
        HashMap<String, String> result = new HashMap<>();
        if(job.isLegit()){
            App.jobList.put(job.getUuid(), job);
            job.broadCast();
            result.put("status", "success");
            result.put("jobId", job.getUuid());
            App.logger.log(Level.INFO, "job received: " + job.getUuid());
        } else {
            job.updateStatus(Statuses.REJECTED);
            job.setErrorMessage("ERROR: Unsupported worker class");

            result.put("status", "failure");
            result.put("errorMessage", job.getErrorMessage());
            App.logger.log(Level.WARNING, "job rejected: " + job.getUuid() + " " + job.getErrorMessage());
        }
        return new JSONObject(result).toString();
    }

    public String handleCommandExecutions(Command cmd){
        HashMap<String, String> result = new HashMap<>();
        if(cmd.isLegit()){
            result.put("status", "success");
            result.put("jobId", cmd.response());
        } else {

            result.put("status", "failure");
            result.put("errorMessage", cmd.helpMessage());
        }
        return new JSONObject(result).toString();
    }
}
