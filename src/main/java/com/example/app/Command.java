package com.example.app;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Command {
    private final ArrayList commands = new ArrayList();
    JSONObject params;

    public Command(JSONObject params){
        commands.add("JOBSTATUS");
        commands.add("SLAVES");
        this.params = params;
    }

    public boolean isLegit(){
        if(!params.has("cmd"))
            return false;
        return commands.contains(params.get("cmd").toString().toUpperCase());
    }

    public String response(){
        switch (params.get("cmd").toString().toUpperCase()){
            case "JOBSTATUS":
                Job job = App.jobList.get(params.get("jobId").toString());
                if(job != null){
                    return job.getStatus().toString();
                } else{
                    return "No job with id: " + params.get("jobId").toString();
                }
            case "SLAVES":
                String result = "";
                for(Slave slave :App.slaveList.values()){
                    String health = slave.isHealthy() ? "Healthy" : "Unhealthy";
                    result = result + slave.getUuid() + ":" + health + " ";
                }
                return result;
        }
        return null;
    }

    public String helpMessage(){
        return usage();
    }

    private String usage(){
        return "newJob <worker> <args...>\njobStatus <jobId>\nslaves\n";
    }
}
