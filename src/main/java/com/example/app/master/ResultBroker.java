package com.example.app.master;

import com.example.app.App;
import com.example.app.Job;
import com.example.app.Task;
import com.example.app.constants.Statuses;
import com.example.app.master.Registrar;
import com.example.app.network.SocketConnection;
import org.json.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;

class ResultBroker implements Runnable {
    private SocketConnection taskResultConnection;

    public ResultBroker(Socket socket) {
        try {
            this.taskResultConnection = new SocketConnection(socket);
        } catch (Exception e){
            App.logger.log(Level.SEVERE, "Failed to open input/output buffers for socket: " + socket);
            App.logger.log(Level.SEVERE, e.getStackTrace().toString());
            try { socket.close(); } catch (IOException ex) {}
        }
    }

    @Override
    public void run() {
        try {
            while(!Thread.currentThread().isInterrupted()){
                String response = taskResultConnection.readMessage();
                JSONObject responseJson = new JSONObject(response);
                boolean jobCompleted = recordResult(responseJson);
                taskResultConnection.writeMessage("OK");
                taskResultConnection.flush();

                if(jobCompleted){
                    taskResultConnection.close();
                    Thread.currentThread().interrupt();
                    return;
                }
            }
        } catch (Exception e) {
            App.logger.log(Level.SEVERE, "Error while processing job alert from the socket: " + taskResultConnection.getSocket());
            App.logger.log(Level.SEVERE, "Error:" + e.getStackTrace());
        } finally {
            try { taskResultConnection.close(); } catch (IOException e) {}
        }
    }

    private boolean recordResult(JSONObject responseJson) {
        String jobId = responseJson.get("jobId").toString();
        String taskId = responseJson.get("taskId").toString();
        App.logger.log(Level.INFO, "Result obtained for job: " + jobId + ", task: " + taskId + " from slave: " + responseJson.get("slaveId"));

        Job job = App.jobList.get(jobId);
        if(job != null){
            Task task = job.getTaskwithId(taskId);
            if(task != null && task.getStatus() != Statuses.COMPLETED){
                task.setResult(responseJson.get("result").toString(), responseJson.get("slaveId").toString());
            } else {
                if(task == null){
                    App.logger.log(Level.WARNING, "No task found with taskId: " + taskId + "for job: " + jobId );
                } else {
                    App.logger.log(Level.WARNING, "Ignoring this result, task already completed by slave: " + task.getRespondingSlaveId());
                }
            }

            job.checkForTaskConvergence();
            if(job.getStatus() == Statuses.TASKS_CONVERGED){
                job.reduceResults();
                return true;
            }
        } else {
            App.logger.log(Level.WARNING, "No job found with jobId: " + jobId );
        }
        return false;
    }
}