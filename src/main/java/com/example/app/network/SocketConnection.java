package com.example.app.network;

import com.example.app.App;

import java.io.*;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.Scanner;
import java.util.logging.Level;

public class SocketConnection {
    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    private String ip;
    private int port;
    private Socket socket;
    private InputStream input;
    private OutputStream output;

    private final String SOCKET_NOT_CONNECTED_MSG = "Socket not connected";

    public SocketConnection(String ip, int port) throws IOException{
        socket = null;
        input = null;
        output = null;
        this.ip = ip;
        this.port = port;
        connect();
    }

    public SocketConnection(Socket socket) throws IOException{
        this.socket = socket;
        this.input = new BufferedInputStream(socket.getInputStream());
        this.output = new BufferedOutputStream(socket.getOutputStream());
    }

    public void connect() throws IOException {
        close();
        Socket skt = null;
        try {
            skt = new Socket(ip, port);
            this.socket = skt;
            this.input = new BufferedInputStream(socket.getInputStream());
            this.output = new BufferedOutputStream(socket.getOutputStream());
        } catch (Exception e){
            App.logger.log(Level.SEVERE, "Connection to host: " + ip + ":" + port + " failed");
            App.logger.log(Level.SEVERE, e.getCause().toString());
        }
    }

    public void close() throws IOException{
        if (socket != null) {
            socket.close();
        }
        if (input != null) {
            input.close();
        }
        if (output != null) {
            output.close();
        }
    }

    public boolean isConnected() {
        if (socket == null){
            return false;
        }
        else if (socket.isConnected()){
            return true;
        }
        else {
            try {
                socket.close();
            } catch (IOException ignored) {
            }
            socket = null;
            return false;
        }
    }

    // Have to implement a way to read with certain timeout
    public String readMessage() throws IOException{
        final byte[] receiveSizeBytes = new byte[4];
        read(receiveSizeBytes);
        final int receiveSize = ByteBuffer.wrap(receiveSizeBytes).getInt();

        final byte[] receiveBytes = new byte[receiveSize];

        int bytesRemaining = receiveSize;
        int offset = 0;
        while (bytesRemaining > 0) {
            final int readCount = read(receiveBytes, offset, bytesRemaining);
            offset += readCount;
            bytesRemaining -= readCount;
        }
        return new String(receiveBytes);
    }

    //TODO: implement timeout scenerio here. Returns empty string if timedout
    public String readMessageWithTiemout(int seconds) throws IOException{
        return readMessage();
    }

    public void writeMessage(String message) throws IOException{
        final byte[] sendBytes = message.getBytes();
        write(ByteBuffer.allocate(4).putInt(sendBytes.length).array());
        write(sendBytes);
        flush();
    }

    public void writeMessageWithoutHeader(String message) throws IOException{
        final byte[] sendBytes = message.getBytes();
        write(sendBytes);
        flush();
    }

    private void read(byte[] bytes) throws IOException {
        if (!isConnected()) {
            throw new IOException(SOCKET_NOT_CONNECTED_MSG);
        }
        input.read(bytes);

    }

    private int read(byte[] bytes, int offset, int remaining) throws IOException {
        if (!isConnected()) {
            throw new IOException(SOCKET_NOT_CONNECTED_MSG);
        }
        return input.read(bytes, offset, remaining);
    }

    public void write(byte[] bytes) throws IOException {
        if (!isConnected()) {
            throw new IOException(SOCKET_NOT_CONNECTED_MSG);
        }
        output.write(bytes);
    }

    public void flush() throws IOException {
        if (!isConnected()) {
            throw new IOException(SOCKET_NOT_CONNECTED_MSG);
        }
        output.flush();
    }
}
