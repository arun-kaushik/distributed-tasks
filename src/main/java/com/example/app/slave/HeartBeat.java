package com.example.app.slave;

import com.example.app.App;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.Scanner;
import java.util.logging.Level;

class HeartBeat implements Runnable {
    private Socket socket;

    public HeartBeat(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        System.out.println("Connected: " + socket);
        try {
            Scanner in = new Scanner(socket.getInputStream());
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            while(in.hasNextLine() && !Thread.currentThread().isInterrupted()){
                String msg = in.nextLine();
                if("PING".equals(msg)){
                    String header = new String(ByteBuffer.allocate(4).putInt(4).array());
                    out.print(header + "PONG");
                    out.flush();
                } else {
                    App.logger.log(Level.WARNING, "Illegal healthcheck message from master: " + msg);
                }
            }
        } catch (Exception e) {
            System.out.println("Error:" + e.getMessage());
        } finally {
            try { socket.close(); } catch (IOException e) {}
            System.out.println("Closed: " + socket);
        }
    }
}