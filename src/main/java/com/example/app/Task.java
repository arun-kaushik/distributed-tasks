package com.example.app;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import com.example.app.constants.Statuses;
import org.json.JSONObject;

public class Task {
    private String uuid;
    private String respondingSlaveId;
    private Statuses status;
    private String workerName;
    private List<String> targetSlaves;
    private HashMap<String, String> taskDetails;
    private String result;
    long startEpoc;
    long endEpoc;

    public String getWorkerName() {
        return workerName;
    }

    public void setWorkerName(String workerName) {
        this.workerName = workerName;
    }

    public String getRespondingSlaveId() {
        return respondingSlaveId;
    }

    public void setRespondingSlaveId(String slaveId) {
        this.respondingSlaveId = slaveId;
    }

    public long getStartEpoc() {
        return startEpoc;
    }

    public void setStartEpoc(long startEpoc) {
        this.startEpoc = startEpoc;
    }

    public long getEndEpoc() {
        return endEpoc;
    }

    public void setEndEpoc(long endEpoc) {
        this.endEpoc = endEpoc;
    }

    public String getUuid(){
        return uuid;
    }

    public void delegateToSlave(Slave slave){
        targetSlaves.add(slave.getUuid());
    }

    public void revokeFromSlave(Slave slave){
        targetSlaves.remove(slave.getUuid());
    }

    public List<String> delegatedToSlaves(){
        return targetSlaves;
    }

    public String getResult() {
        return result;
    }

    public synchronized void setResult(String result, String respondingSlaveIdveId) {
        updateStatus(Statuses.COMPLETED);
        this.result = result;
        this.respondingSlaveId = respondingSlaveIdveId;
    }

    public void updateStatus(Statuses status){
        this.status = status;
    }

    public synchronized Statuses getStatus() {
        return status;
    }

    public void addDetail(String key, String val){
        this.taskDetails.put(key, val);
    }

    public String getDetail(String key){
        return this.taskDetails.get(key);
    }

    public Task(HashMap<String, String > taskDetails){
        this.result = null;
        this.taskDetails = taskDetails;
        this.uuid = UUID.randomUUID().toString();
        this.startEpoc = 122323L;
        this.endEpoc = 132323L;
        this.targetSlaves = new LinkedList<>();
        this.respondingSlaveId = null;
        updateStatus(Statuses.RECEIVED);
    }

    public String toJson(){
        HashMap<String, String> map = new HashMap<String, String >();
        map.put("workerName", this.workerName);
        map.put("uuid", this.uuid);
        map.put("slaveId", this.respondingSlaveId);
        map.put("startEpoc", Long.toString(startEpoc));
        map.put("endEpoc", Long.toString(endEpoc));
        map.put("taskDetails", new JSONObject(taskDetails).toString());

        JSONObject json = new JSONObject(map);
        return json.toString();
    }
}
