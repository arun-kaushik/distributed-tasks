package com.example.worker;

import com.example.app.App;
import com.example.app.Slave;
import com.example.app.Job;
import com.example.app.Task;
import com.example.app.constants.Statuses;
import org.json.JSONObject;

import java.io.*;
import java.util.HashMap;
import java.util.logging.Level;

public class Capitalizer implements WorkerBehavior {
    private JSONObject params;
    private final String resultDir = "/Users/arun.kaushik/Documents/distributed-tasks/jobs/";
    private final String resultFileName = "result.txt";

    public Capitalizer(JSONObject params) {
        this.params = params;
    }
    public Capitalizer() {
        this.params = null;
    }

    @Override
    public void splitJob(Job job) throws Exception {
        JSONObject params = job.getParams();
        String filePath = params.get("filePath").toString();
        int bufferLen = 1024;
        try {
            char[] myBuffer = new char[bufferLen];
            int bytesRead = 0;
            BufferedReader in = new BufferedReader(new FileReader(filePath));
            while ((bytesRead = in.read(myBuffer,0,bufferLen)) != -1) {
                HashMap<String, String > details = new HashMap<String, String>();
                details.put("text", new String(myBuffer, 0, bytesRead));
                job.addTask(new Task(details));
            }
        } catch (FileNotFoundException fne) {
            App.logger.log(Level.SEVERE, "No File found at: " + filePath);
            throw fne;
        } catch (IOException ioE) {
            App.logger.log(Level.SEVERE, ioE.getMessage());
            throw ioE;
        }

    }

    @Override
    public void reduceResults(Job job) throws Exception {
        job.updateStatus(Statuses.REDUCING_RESULTS);
        String finalResult = "";
        try {
            for(String taskId : job.getTaskOrder()){
                Task task = job.getTaskwithId(taskId);
                finalResult = finalResult + task.getResult();
            }

            String directoryName = resultDir.concat(job.getUuid());

            File directory = new File(directoryName);
            if (! directory.exists()){
                directory.mkdirs();
            }

            File file = new File(directoryName + "/" + resultFileName);
            try{
                FileWriter fw = new FileWriter(file.getAbsoluteFile());
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write(finalResult);
                bw.close();
            }
            catch (IOException e){
                App.logger.log(Level.WARNING, "Failed to write the results of job: " + job.getUuid() + " to: " + file.getAbsoluteFile() );
                App.logger.log(Level.WARNING, e.getStackTrace().toString());
            }

        } catch (Exception e){
            App.logger.log(Level.WARNING, "Failed to reduce the results for job: " + job.getUuid());
            App.logger.log(Level.WARNING, e.getStackTrace().toString());
        }
    }

    @Override
    public String perform(String taskDetails) {
        JSONObject taskObj = new JSONObject(taskDetails);
        return taskObj.get("text").toString().toUpperCase();
    }
}