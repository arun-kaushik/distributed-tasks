package com.example.app;

import com.example.app.master.*;
import com.example.app.slave.HealthCheckResponder;
import com.example.app.slave.JobAlertReceiver;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class App
{
    public static Logger logger = Logger.getLogger(App.class.getName());

    public static final int MASTER_SLAVE_REGISTRATION_PORT = 59894;
    public static final int MASTER_JOBS_RECEIVER_PORT = 59895;
    public static final int MASTER_RESULT_RECEIVER_PORT = 59896;
    public static final int MASTER_REQUEST_RECEIVER_PORT = 59897;
    public static final int SLAVE_HEALTHCHECK_PORT = 59898;
    public static final int SLAVE_JOB_ALERT_RECEIVER_PORT = 59899;
//    public static ArrayList<Slave> slaveList = new ArrayList<Slave>();
    public static HashMap<String, Slave> slaveList = new HashMap<String, Slave>();
    public static HashMap<String, Job> jobList = new HashMap<String, Job>();
    public static String slaveId = null;
    public static String masterIp = null;

    public static void main(String[] args) throws Exception {
        //logger.addHandler(new ConsoleHandler());
        logger.setLevel(Level.FINEST);

        logger.log(Level.INFO, "Args is:" +  args[0]);
        if(args.length < 1){
            throw new IllegalArgumentException("Not a valid argument list.\n Usage: master Or slave <master ip> ");
        } else if( "master".equals( args[0].toLowerCase() ) ){
            spinAsMaster();
        } else if( "slave".equals( args[0].toLowerCase() ) ){
            if(args.length != 4){
                throw new IllegalArgumentException("Not a valid argument list.\n Usage: slave <master ip> <healthCheckPort <JobAlertPort>>");
            }
            spinAsSlave(args);
        } else {
            throw new IllegalArgumentException("Not a valid argument list.\n Usage: master Or slave <master ip> ");
        }
    }

    private static void spinAsMaster(){
        logger.log(Level.INFO, "Starting 'distributed-tasks' server in master mode.");

        Thread registrarThread = new Thread(new Registrar());
        registrarThread.setName("registrarThread");
        registrarThread.start();

        Thread healthCheckThread = new Thread(new HealthMonitor());
        healthCheckThread.setName("healthCheckThread");
        healthCheckThread.start();

        Thread jobsThread = new Thread(new JobReceiver());
        jobsThread.setName("jobsThread");
        jobsThread.start();

        Thread resultThread = new Thread(new ResultReceiver());
        resultThread.setName("resultThread");
        resultThread.start();

        Thread taskRequestThread = new Thread(new TaskRequestReceiver());
        taskRequestThread.setName("taskRequestReceiver");
        taskRequestThread.start();
    }

    private static void spinAsSlave(String[] args){
        logger.log(Level.INFO, "Starting 'distributed-tasks' server in slave mode.");
        App.masterIp = args[1];
        Integer healthCheckPort = Integer.parseInt(args[2]);
        Integer jobAlertPort = Integer.parseInt(args[3]);

        Thread healthCheckThread = new Thread(new HealthCheckResponder(healthCheckPort));
        healthCheckThread.setName("healthCheckThread");
        healthCheckThread.start();

        Thread taskReceiver = new Thread(new JobAlertReceiver(jobAlertPort));
        taskReceiver.setName("taskReceiver");
        taskReceiver.start();

        if(registerToMaster(masterIp, healthCheckPort, jobAlertPort)){
            App.logger.log(Level.INFO, "Ready to server tasks...");
        } else {
            healthCheckThread.interrupt();
            taskReceiver.interrupt();
            logger.log(Level.SEVERE, "Exiting slave");
        }
    }

    private static boolean registerToMaster(String masterIp, int healthCheckPort, int jobAlertPort){
        Socket socket = null;
        try {
            socket = new Socket(masterIp, MASTER_SLAVE_REGISTRATION_PORT);
            Scanner in = new Scanner(socket.getInputStream());
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            out.println(healthCheckPort + " " + jobAlertPort);
            slaveId = in.nextLine();
        } catch (Exception e){
            logger.log(Level.SEVERE, "Failed to register to master at: " + masterIp + ":" + MASTER_SLAVE_REGISTRATION_PORT);
        } finally {
            try { socket.close(); } catch (IOException e) {}
            logger.log(Level.INFO, "Socket closed: " + socket);
        }

        if(slaveId != null){
            logger.log(Level.INFO, "Registration to master is successful with slaveid: " + slaveId);
            return true;
        } else {
            logger.log(Level.SEVERE, "Registration to master failed");
            return false;
        }
    }
}
