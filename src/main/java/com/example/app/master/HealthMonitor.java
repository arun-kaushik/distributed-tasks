package com.example.app.master;

import com.example.app.App;
import com.example.app.Slave;

import java.util.logging.Level;

public class HealthMonitor implements Runnable{

    public HealthMonitor() {
    }
    @Override
    public void run() {
        try {
            while(true){
                for(Slave slave : App.slaveList.values()){
                    //App.logger.log(Level.INFO, "Checking health of slave: " + slave.getUuid());
                    slave.checkHealth();
                }
                Thread.sleep(5 * 1000);
            }
        } catch(InterruptedException e){
            return;
        }

    }
}
