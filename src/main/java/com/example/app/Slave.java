package com.example.app;

import com.example.app.network.SocketConnection;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.UUID;
import java.util.logging.Level;

public class Slave {
    private final int MAX_UNHEALTHY_RESPONSES = 10;
    private String uuid;
    private String ip;
    private int unhealthyCount;
    private SocketConnection healthCheckConnection;
    private SocketConnection jobBroadCastConnection;

    public Slave(String ip, String ports) {
        App.logger.log(Level.INFO, "Slave IP and ports are: " + ip + " " + ports);
        String[] slavePorts = ports.split("\\s+");
        this.uuid = UUID.randomUUID().toString();
        this.ip = ip;
        this.unhealthyCount = 0;
        try {
            healthCheckConnection = new SocketConnection(ip, Integer.parseInt(slavePorts[0]));
            jobBroadCastConnection = new SocketConnection(ip, Integer.parseInt(slavePorts[1]));
        } catch (IOException e){
            App.logger.log(Level.SEVERE, Thread.currentThread().getName() + ": Connection to Slave: " + ip + " failed");
            App.logger.log(Level.SEVERE, e.getCause().toString());
        }
    }

    public String getUuid() {
        return uuid;
    }

    public String getIp() {
        return ip;
    }

    public boolean isHealthy(){
        return unhealthyCount == 0;
    }

    public boolean isDead(){
        return unhealthyCount >= MAX_UNHEALTHY_RESPONSES;
    }

    private void degradeHealth(){
        unhealthyCount++;
    }

    // Checks the health of the slave and mark is unhealthy if not responding.
    // If slave stays unhealthy for MAX_UNHEALTHY_RESPONSES health checks, delete it from the slave list
    public void checkHealth(){
        try {
            if(!healthCheckConnection.isConnected()){
                healthCheckConnection.connect();
            }

            healthCheckConnection.writeMessageWithoutHeader("PING\n");
            String msg = healthCheckConnection.readMessageWithTiemout(10);
            if("PONG".equals(msg)){
                unhealthyCount = 0;
                //App.logger.log(Level.INFO, "Slave: " + uuid +  " is healthy");
            } else {
                App.logger.log(Level.WARNING, "Slave: " + uuid +  " is unhealthy");
                unhealthyCount++;
            }
        } catch (IOException e) {
            App.logger.log(Level.WARNING, "Slave: " + uuid +  " is unhealthy");
            unhealthyCount++;
            App.logger.log(Level.SEVERE, Thread.currentThread().getName() + ": Connection to Slave: " + ip + " failed");
            if(e.getCause() != null){
                App.logger.log(Level.SEVERE, e.getCause().toString());
            } else {
                App.logger.log(Level.SEVERE, "Exception: No-Info");
            }
        }
    }

    public boolean sendJobAlert(Job job){
        try {
            jobBroadCastConnection.writeMessage(job.getUuid());
            String response = jobBroadCastConnection.readMessage();
            if("OK".equals(response)){
                App.logger.log(Level.FINE, "Job: " + job.getUuid() + "is acknoledged by slave: " + uuid);
                return true;
            } else {
                App.logger.log(Level.WARNING, "Slave: " + uuid + "failed to acknoledge job: " + job.getUuid());
                return false;
            }
        } catch (IOException e){
            App.logger.log(Level.WARNING, "Failed to broadcast job: " + job.getUuid());
            return  false;
        }
    }

    public boolean sendTask(Task task){
       return true;
    }

//    private void waitForHeartBeatWithTimeout(){
//        int attempts = 10;
//        try {
//            while(attempts-- > 0){
//                if(socketInput.hasNextLine()){
//                    if(socketInput.nextLine() == "PONG"){
//                        unhealthyCount = 0;
//                        return;
//                    } else {
//                        unhealthyCount++;
//                        return;
//                    }
//                } else {
//                    Thread.sleep(1 * 700);
//                }
//            }
//            unhealthyCount++;
//        } catch (Exception e) {
//            return;
//        }
//    }

}
