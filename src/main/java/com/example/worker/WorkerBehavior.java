package com.example.worker;

import com.example.app.Slave;
import com.example.app.Job;
import com.example.app.Task;
import org.json.JSONObject;

public interface WorkerBehavior {
    String perform(String taskDetails) throws Exception;

    // Responsible to divide the Job into independent smaller chunks of work unit (Task).
    void splitJob(Job job)throws Exception;

    // Responsible to reduce the results of all Tasks into final result
    void reduceResults(Job job) throws Exception;
}
