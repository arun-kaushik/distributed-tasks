package com.example.app.slave;

import com.example.app.App;

import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class JobAlertReceiver implements Runnable{
    int jobAlertPort;
    public JobAlertReceiver(Integer jobAlertPort) {
        this.jobAlertPort = jobAlertPort;
    }

    @Override
    public void run() {
        try {
            ServerSocket listener = new ServerSocket(jobAlertPort);
            ExecutorService taskExecutorPool = Executors.newFixedThreadPool(5);
            while (true) {
                taskExecutorPool.execute(new JobAlertProcessor(listener.accept()));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
