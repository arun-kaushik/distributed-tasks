package com.example.app.master;

import com.example.app.App;
import com.example.app.Job;
import com.example.app.Marshal;
import com.example.app.constants.Statuses;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;

public class JobBroker implements Runnable {
    private Socket socket;

    public JobBroker(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            Scanner in = new Scanner(socket.getInputStream());
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            String response = new Marshal(in.nextLine()).execute();
            out.println(response);
            in.nextLine();
        } catch (Exception e) {
            App.logger.log(Level.SEVERE, "Error receiving new job: " + e.getMessage());
        } finally {
            try { socket.close(); } catch (IOException e) {}
            App.logger.log(Level.INFO, "Socket closed: " + socket);
        }
    }
}