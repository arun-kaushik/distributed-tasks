package com.example.app;

import com.example.app.constants.Statuses;
import com.example.worker.Capitalizer;
import com.example.worker.HelloWorld;
import com.example.worker.JenkinsScriptExecutor;
import com.example.worker.WorkerBehavior;
import org.json.JSONObject;

import java.util.*;
import java.util.logging.Level;

public class Job {
    private JSONObject params;
    private String errorMessage;
    private String uuid;
    private Statuses status;
    private String workerName;
    private LinkedList<String> timeline;
    private LinkedList<String> slavesReceivedJobAlert;
    WorkerBehavior workerBehavior;
    private HashMap<String, Task> tasks;

    private ArrayList<String> taskOrder;

    public Job(JSONObject params){
        timeline = new LinkedList<String>();
        slavesReceivedJobAlert = new LinkedList<String>();
        updateStatus(Statuses.RECEIVED);

        this.params = params;
        setWorkerBehavior();
        this.errorMessage = null;
        this.uuid = UUID.randomUUID().toString();
        this.tasks = new HashMap<>();
        this.taskOrder = new ArrayList<>();
    }

    public ArrayList<String> getTaskOrder() {
        return taskOrder;
    }

    public void setTaskOrder(ArrayList<String> taskOrder) {
        this.taskOrder = taskOrder;
    }

    public JSONObject getParams() {
        return params;
    }

    public ArrayList<Task> getTasks() {
        return new ArrayList(tasks.values());
    }

    public Task getTaskwithId(String taskId){
        return tasks.get(taskId);
    }

    public boolean isLegit(){
        return workerBehavior == null ? false : true;
    }

    public String getErrorMessage(){
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage){
        this.errorMessage = errorMessage;
    }

    public String getUuid(){
        return uuid;
    }

    public Statuses getStatus() {
        return status;
    }

    public void updateStatus(Statuses status){
        this.status = status;
        timeline.add(status.toString() + " at: " + System.nanoTime());
        App.logger.log(Level.INFO, "Status of job: " + this.uuid + " updated to: " + status.toString());
    }

    public void addTask(Task task){
        task.setWorkerName(workerName);
        this.tasks.put(task.getUuid(), task);
        taskOrder.add(task.getUuid());
    }

    public void broadCast(){
        splitWorkIntoTasks();

        App.logger.log(Level.FINE, "Broadcasting job: " + uuid + "to slaves");
        for(Slave slave : App.slaveList.values()){
            if(slave.isHealthy() && slave.sendJobAlert(this)){
                slavesReceivedJobAlert.add(slave.getUuid());
            } else {
                App.logger.log(Level.WARNING, "Not sending job alert to unhealthy slave: " + slave.getUuid());
            }
        }
        if(slavesReceivedJobAlert.size() > 0){
            App.logger.log(Level.FINE, "Job: " + uuid + " broadcasted to " + slavesReceivedJobAlert.size() + " slaves");
            updateStatus(Statuses.BROAD_CASTED);
        }
    }

    public synchronized Task assignTaskToSlave(Slave slave){
        for(int i = 1; i < 10; i++) {
            for(Task task : this.tasks.values()){
                if(task.getStatus() != Statuses.COMPLETED && task.delegatedToSlaves().size() < i){
                    task.delegateToSlave(slave);
                    return task;
                }
            }
        }
        return null;
    }

    public synchronized void checkForTaskConvergence(){
        for(Task task : this.tasks.values()){
            if(task.getStatus() != Statuses.COMPLETED)
                return;
        }
        this.updateStatus(Statuses.TASKS_CONVERGED);
    }

    public void reduceResults(){
        App.logger.log(Level.INFO, "Reducing results for job: " + this.uuid);
        try{
            this.workerBehavior.reduceResults(this);
            updateStatus(Statuses.COMPLETED);
            App.logger.log(Level.INFO, "Successfully reduced results for job: " + this.uuid);
        } catch (Exception e){
            updateStatus(Statuses.FAILED);
            this.errorMessage = e.getMessage();
            App.logger.log(Level.WARNING, "Failed to reduce results for job: " + this.uuid);
            App.logger.log(Level.WARNING, e.getStackTrace().toString());
        }
    }

    private void setWorkerBehavior(){
        this.workerName = params.get("worker").toString().toUpperCase();
        switch (workerName){
            case "HELLOWORLD":
                this.workerBehavior = new HelloWorld(params);
                break;
            case "CAPITALIZER":
                this.workerBehavior = new Capitalizer(params);
                break;
            case "JENKINSSCRIPTEXECUTOR":
                this.workerBehavior = new JenkinsScriptExecutor(params);
                break;
            default:
                this.errorMessage = "Illegal worker class";
                this.workerBehavior = null;
        }
    }

    private void splitWorkIntoTasks(){
        App.logger.log(Level.FINE, "Splitting job: " + uuid);
        try {
            workerBehavior.splitJob(this);
            App.logger.log(Level.INFO, "Successfully split job: " + uuid +  " into " + tasks.size() + " tasks");
        } catch (Exception e){
            App.logger.log(Level.WARNING, "Failed to split job: " + uuid +  " into tasks");
        }

    }
}
