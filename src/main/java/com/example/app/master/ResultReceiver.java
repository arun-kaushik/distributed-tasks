package com.example.app.master;

import com.example.app.App;

import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ResultReceiver implements Runnable{

    public ResultReceiver() {
    }
    @Override
    public void run() {
        try {
            ServerSocket listener = new ServerSocket(App.MASTER_RESULT_RECEIVER_PORT);
            ExecutorService registrarPool = Executors.newCachedThreadPool();
            while (true) {
                registrarPool.execute(new ResultBroker(listener.accept()));
            }
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
