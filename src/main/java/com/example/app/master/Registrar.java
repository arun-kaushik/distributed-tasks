package com.example.app.master;

import com.example.app.App;

import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;

public class Registrar implements Runnable{

    public Registrar() {
    }
    @Override
    public void run() {
        int count = 1;
        try {
            ServerSocket listener = new ServerSocket(App.MASTER_SLAVE_REGISTRATION_PORT);
            ExecutorService registrarPool = Executors.newFixedThreadPool(5);
            while (true) {
                App.logger.log(Level.INFO, "Slave trying to connect: " + count++);
                registrarPool.execute(new ConnectionBroker(listener.accept()));
            }
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
