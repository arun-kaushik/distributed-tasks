package com.example.app.master;


import com.example.app.App;
import com.example.app.Job;
import com.example.app.Slave;
import com.example.app.constants.Statuses;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;

public class ConnectionBroker implements Runnable {
    private Socket socket;

    public ConnectionBroker(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        System.out.println("Connected: " + socket);
        try {
            Scanner in = new Scanner(socket.getInputStream());
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            String msg = in.nextLine();
            App.logger.log(Level.INFO, "Registration message receieved from slave: " + msg);
            Slave slave = new Slave(socket.getInetAddress().toString().replace("/",""), msg);
            App.slaveList.put(slave.getUuid(), slave);
            // This is newly registered slave, send it job alerts for jobs which aren't completed.
            for(Job job : App.jobList.values()){
                if(job.getStatus() == Statuses.BROAD_CASTED || job.getStatus() == Statuses.IN_PROGRESS){
                    slave.sendJobAlert(job);
                }
            }
            out.println(slave.getUuid());
        } catch (Exception e) {
            System.out.println("Error:" + e.getMessage());
        } finally {
            try { socket.close(); } catch (IOException e) {}
            System.out.println("Closed: " + socket);
        }
    }
}