package com.example.app.slave;

import com.example.app.App;

import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class HealthCheckResponder implements Runnable{
    int healthCheckPort;

    public HealthCheckResponder(Integer healthCheckPort) {
        this.healthCheckPort = healthCheckPort;
    }

    @Override
    public void run() {
        try {
            ServerSocket listener = new ServerSocket(healthCheckPort);
            ExecutorService healthCheckPool = Executors.newFixedThreadPool(1);
            while (true) {
                healthCheckPool.execute(new HeartBeat(listener.accept()));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
